$('.form__item-visible').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('form__item-visible--active');
  var input = $(this).closest('.form__item').find('input');
  if (input.attr('type') == 'password') {
    input.attr('type', 'text');
  }
  else if (input.attr('type') == 'text') {
    input.attr('type', 'password');
  }
})