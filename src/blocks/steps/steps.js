$('.steps__next').click(function(e) {
  e.preventDefault();
  var index = $(this).closest('.steps__content-item').index();
  $('.steps__item').eq(index).removeClass('steps__item--active');
  $('.steps__content-item').eq(index).removeClass('steps__content-item--active');
  $('.steps__item').eq(index + 1).addClass('steps__item--active');
  $('.steps__content-item').eq(index + 1).addClass('steps__content-item--active');
});

$('.steps__prev').click(function(e) {
  e.preventDefault();
  var index = $(this).closest('.steps__content-item').index();
  $('.steps__item').eq(index).removeClass('steps__item--active');
  $('.steps__content-item').eq(index).removeClass('steps__content-item--active');
  $('.steps__item').eq(index - 1).addClass('steps__item--active');
  $('.steps__content-item').eq(index - 1).addClass('steps__content-item--active');
});