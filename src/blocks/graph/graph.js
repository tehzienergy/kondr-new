if ($('.graph').length) {

  const graphSimple = {
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
      legend: {
        display: false,
      },
      tooltip: {
        enabled: false
      }
    },
    elements: {
      point: {
        radius: 0
      }
    },
    scales: {
      x: {
        display: false,
      },
      y: {
        display: false,
      }
    }
  };

  const graphSimpleDotUp = {
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
      legend: {
        display: false,
      },
      tooltip: {
        enabled: false
      }
    },
    elements: {
      point: {
        radius: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2.5],
        backgroundColor: '#3D8AFF'
      }
    },
    scales: {
      x: {
        display: false,
      },
      y: {
        display: false,
      }
    }
  };

  const chartDown = new Chart(
    $('#graph-down'), {
      type: 'line',
      data: {
        labels: [
          '1',
          '2',
          '3',
          '4',
          '5',
          '6',
          '7',
          '8',
          '9',
          '10',
          '11',
          '12',
        ],
        datasets: [{
          borderColor: '#DC3545',
          data: [0, 10, 5, 2, 20, 30, 45, 10, 10, 5, 2, 20],
          lineTension: 0.5,
          borderWidth: 2
        }]
      },
      options: graphSimple
    }
  );

  const chartUp = new Chart(
    $('#graph-up'), {
      type: 'line',
      data: {
        labels: [
          '1',
          '2',
          '3',
          '4',
          '5',
          '6',
          '7',
          '8',
          '9',
          '10',
          '11',
          '12',
        ],
        datasets: [{
          borderColor: '#3D8AFF',
          data: [0, 20, 15, 17, 22, 16, 24, 18, 30, 25, 22, 28],
          lineTension: 0.5,
          borderWidth: 2,
        }]
      },
      options: graphSimple
    }
  );

  if ($('#graph-filled').length) {
    var ctx = document.getElementById("graph-filled").getContext("2d");
    var gradient = ctx.createLinearGradient(0, 0, 0, 400);
    gradient.addColorStop(0.0, 'rgba(61, 138, 255, 0.3)');
    gradient.addColorStop(0.83, 'rgba(61, 138, 255, 0)');

    const chartFilled = new Chart(
      $('#graph-filled'), {
        type: 'line',
        data: {
          labels: [
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            '10',
            '11',
            '12',
          ],
          datasets: [{
            borderColor: '#3D8AFF',
            data: [0, 20, 15, 17, 22, 16, 24, 18, 30, 25, 22, 28],
            lineTension: 0.2,
            borderWidth: 2,
            fill: true,
            backgroundColor: gradient
          }]
        },
        options: graphSimple
      }
    );
  }

  $('.graph__canvas--profit-up').each(function () {
    const chartProfitUp = new Chart(
      $(this), {
        type: 'line',
        data: {
          labels: [
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            '10',
            '11',
            '12',
            '13',
            '14',
            '15',
            '16',
            '17',
            '18',
            '19',
            '20',
            '21',
            '22',
            '23',
            '24',
            '25',
            '26',
            '27',
            '28',
            '29',
            '30',
            '31',
            '32',
            '33',
            '34',
            '35',
            '36',
            '37',
            '38',
            '39',
            '40',
          ],
          datasets: [{
            borderColor: '#3D8AFF',
            data: [25, 20, 22, 20, 18, 21, 19, 16, 17, 23, 10, 11, 22, 18, 19, 21, 20, 23, 23, 27, 25, 20, 22, 20, 18, 21, 19, 16, 17, 23, 22, 16, 22, 18, 19, 21, 20, 23, 23, 27],
            lineTension: 0.05,
            borderWidth: 2,
          }]
        },
        options: graphSimpleDotUp
      }
    );
  });
  
  

  const doughnutSimple = {
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
      legend: {
        display: false,
      },
      tooltip: {
        enabled: false
      }
    },
    elements: {
      point: {
        radius: 0
      }
    },
    scales: {
      x: {
        display: false,
      },
      y: {
        display: false,
      }
    }
  };

  $('.graph__canvas--doughnut').each(function () {
    var value = $(this).closest('.graph').data('amount');
    
    const chartDoughnut = new Chart(
      $(this), {
        type: 'doughnut',
        data: {
          datasets: [{
            data: [value, 100-value],
            backgroundColor: ['#3D8AFF', '#1d1e28'],
            borderColor: 'transparent',
            borderWidth: 0,
            borderRadius: 15,
            cutout: '85%'
          }]
        },
        options: {
          responsive: true,
          plugins: {
            legend: {
              display: false
            },
            tooltip: {
              enabled: false
            },
            title: {
              display: false,
              text: 'Chart.js Doughnut Chart'
            }
          }
        },
      }
    );
  });
}